/** @type {HTMLCanvasElement} */

var canvas, settingBlock, fontBlock, panSize, colorPicker, colorText, fontSize, fontStyle, _text, _bold;
var c = canvas.getContext("2d");
var draw = false, sbMove = false, fbMove = false;
var blockX, blockY, detX, detY, msX, msY;
var color;

var pencil, eraser, text, line, circle, triangle, rectangle, upload;

function sbdown(){
    sbMove = true;
    blockX = window.getComputedStyle(settingBlock).getPropertyValue("left");
    blockY = window.getComputedStyle(settingBlock).getPropertyValue("top");

    msX = window.event.clientX;
    msY = window.event.clientY;
}

function fbdown(){
    fbMove = true;
    blockX = window.getComputedStyle(fontBlock).getPropertyValue("left");
    blockY = window.getComputedStyle(fontBlock).getPropertyValue("top");

    msX = window.event.clientX;
    msY = window.event.clientY;
}

function bmv(){
    if(sbMove){
        detX = window.event.clientX - msX;
        detY = window.event.clientY - msY;
        settingBlock.style.left = parseInt(blockX) + detX + "px";
        settingBlock.style.top = parseInt(blockY) + detY + "px";
    }else if(fbMove){
        detX = window.event.clientX - msX;
        detY = window.event.clientY - msY;
        fontBlock.style.left = parseInt(blockX) + detX + "px";
        fontBlock.style.top = parseInt(blockY) + detY + "px";
    }
}

function bup(){
    sbMove = false;
    fbMove = false;
}

function setLineWidth(){
    c.lineWidth = panSize.value;
}

function colorPkAct(){
    color = colorPicker.value;
    colorText.value = color;
    if(eraser.checked != true){
        c.strokeStyle = color;
    }
}

function colorTxAct(){
    if(colorText.value.length == 7){
        colorPicker.value = colorText.value;
        color = colorText.value;
    }else{
        colorText.value = color;
        alert("Wrong color");
    }
}

function changeColor(){
    if(event.keyCode == 13){
        colorTxAct();
        colorText.blur();
    }
}

function clearCanvas(){
    _history.redo_list = [];
    _history.undo_list = [];
    _history.reg_list = [];

    c.clearRect(0, 0, canvas.width, canvas.height);

    c.lineWidth = 1;
    c.strokeStyle = "#ffffff";

    c.beginPath();
    c.moveTo(0, 0);
    c.lineTo(canvas.width, 0);
    c.lineTo(canvas.width, canvas.height);
    c.lineTo(0, canvas.height);
    c.fillStyle = "#ffffff";
    c.fill();
    c.stroke();
    c.closePath();
}

function check(){
    canvas.style.cursor = "crosshair";
    c.strokeStyle = color;
    setLineWidth();
    c.lineCap = "round";
    c.lineJoin = "round";
    fontBlock.style.visibility = "hidden";

    if(pencil.checked == true){
        canvas.style.cursor = "url('img/pencil_ms.png') 0 100, auto";
    }else if(eraser.checked == true){
        c.strokeStyle = "#ffffff";
    }else if(text.checked == true){
        fontBlock.style.visibility = "visible";
        setCanvasFont();
    }else if(circle.checked || triangle.checked || rectangle.checked){
        c.lineCap = "square";
        c.lineJoin = "miter";
    }
}

 function uploadFile(){
    var reader = new FileReader();
    reader.onload = ()=>{
        var img = new Image();
        img.src = reader.result;
        img.onload = ()=>{
            c.drawImage(img, 0, 0);
        }
    }
    reader.readAsDataURL(upload.files[0]);
}

function setFontSize(){
    document.getElementById("fontSize_text").innerHTML = fontSize.value;
    setCanvasFont();
}

function setCanvasFont(){
    if(_bold.checked){
        c.font = "bold " + fontSize.value + "px " + fontStyle.value;
        console.log("bold " + fontSize.value + "px " + fontStyle.value);

    }else{
        c.font = "normal " + fontSize.value + "px " + fontStyle.value;
        console.log("normal " + fontSize.value + "px " + fontStyle.value);

    }
}

