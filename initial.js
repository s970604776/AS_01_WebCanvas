
function init(){
    canvas = document.getElementById("c");
    c = canvas.getContext("2d");
    canvas.addEventListener("mousedown", c_md);
    canvas.addEventListener("mousemove", mv);
    canvas.addEventListener("mouseup", c_mu);
    canvas.addEventListener("mouseout", ()=>{draw=false;c.closePath();text_outf()});

    settingBlock = document.getElementById("settingBlock");
    fontBlock = document.getElementById("fontBlock");
    
    panSize = document.getElementById("pixel");

    colorPicker = document.getElementById("colorPicker");
    colorText = document.getElementById("colorText");
    color = "#000000";

    pencil = document.getElementById("pencil");
    eraser = document.getElementById("eraser");
    text = document.getElementById("text");
    rectangle = document.getElementById("rectangle");
    circle = document.getElementById("circle");
    triangle = document.getElementById("triangle");
    line = document.getElementById("line");
    upload = document.getElementById("upload");
    fontSize = document.getElementById("fontSize");
    fontStyle = document.getElementById("fontStyle");
    _text = document.getElementById("_text");
    _bold = document.getElementById("text_bold");
    console.log(_bold)
    

    fontStyle.addEventListener("change", setCanvasFont);

    clearCanvas();
    check();
}

