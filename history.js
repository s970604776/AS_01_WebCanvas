var _history = {
    redo_list: [],
    undo_list: [],
    reg_list: [],

    undo_list_lastElement: new Image(),
    reg_Element: new Image(),


    saveState: function(canvas, str, clearRedo = false){
        if(clearRedo){
            this.redo_list = [];
        }
        if(str=="undo") {
            this.undo_list.push(canvas.toDataURL()); 
            this.undo_list_lastElement.src = canvas.toDataURL();
        }
        else this.redo_list.push(canvas.toDataURL());
    },

    undo: function(canvas, c){
        if(this.undo_list.length){
            this.saveState(canvas, "redo");
            var img = new Image();
            img.src = this.undo_list.pop();
            img.onload = ()=>{
                c.clearRect(0, 0, canvas.width, canvas.height);
                c.drawImage(img, 0, 0);
            }

        }
    },

    redo: function(canvas, c){
        if(this.redo_list.length){
            this.saveState(canvas, "undo");
            var img = new Image();
            img.src = this.redo_list.pop();
            img.onload = ()=>{
                c.clearRect(0, 0, canvas.width, canvas.height);
                c.drawImage(img, 0, 0);
            }
        }
    },

    loop: function(canvas, c){
        c.clearRect(0, 0, canvas.width, canvas.height);
        c.drawImage(this.undo_list_lastElement, 0, 0);
    }
};