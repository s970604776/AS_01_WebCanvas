var imgDraw;

function c_md(){
    _history.saveState(canvas, "undo", true);
    imgDraw = false;

    if(pencil.checked || eraser.checked){
        draw = true;
        c.beginPath();
        c.moveTo(event.offsetX, event.offsetY);
        c.lineTo(event.offsetX, event.offsetY);
        c.stroke();
    }else if(text.checked&& _text.value != ""){
        //_history.undo_list.push(_history.reg_list.pop());
        _history.reg_list = [];
    }else if(line.checked || rectangle.checked || circle.checked || triangle.checked){
        draw = true;
        msX = event.offsetX;
        msY = event.offsetY;
    }
}

function mv(){
    if(pencil.checked || eraser.checked){
        if(draw){
            c.lineTo(event.offsetX, event.offsetY);
            c.stroke();
        }
    }else if(line.checked){
        if(draw){
            _history.loop(canvas, c);
            c.beginPath();
            c.moveTo(msX, msY);
            c.lineTo(event.offsetX, event.offsetY);
            c.stroke();
            c.closePath();
            imgDraw = true;
        }
    }else if(rectangle.checked){
        if(draw){
            _history.loop(canvas, c);
            c.lineWidth = 1;

            c.beginPath();
            c.rect(msX, msY, event.offsetX - msX, event.offsetY - msY);
            c.fillStyle = color;
            c.fill();
            c.stroke();
            c.closePath();
            imgDraw = true;
        }
    }else if(circle.checked){
        if(draw){
            _history.loop(canvas, c);
            c.lineWidth = 1;

            c.beginPath();
            // X>Y?
            var max_r = event.offsetX - msX;
            if(max_r < 0) max_r *= -1;
            var y_ = event.offsetY - msY;
            if(y_ < 0) y_ *= -1;
            if(max_r < y_) max_r = y_;

            c.arc(msX, msY, max_r, 0, 2 * Math.PI);            
            c.fillStyle = color;
            c.fill();
            c.stroke();
            c.closePath();
            imgDraw = true;
        }
    }else if(triangle.checked){
        if(draw){
            _history.loop(canvas, c);
            c.lineWidth = 1;

            c.beginPath();

            var width_x = event.offsetX - msX;
            
            c.moveTo(msX + width_x / 2, msY);
            c.lineTo(event.offsetX, event.offsetY);
            c.lineTo(msX, event.offsetY);
            c.lineTo(msX + width_x / 2, msY);
            c.fillStyle = color;
            c.fill();
            c.stroke();
            c.closePath();
            imgDraw = true;
        }
    }else if(text.checked){
        if(_history.reg_list.length == 0){
            _history.reg_list.push(canvas.toDataURL());
            _history.reg_Element.src = _history.reg_list[0];
            _history.undo_list.push(canvas.toDataURL());
        }else {
            c.drawImage(_history.reg_Element, 0, 0);

            c.fillStyle = color;
            c.fillText(_text.value, event.offsetX, event.offsetY);
        }
    }
}

function c_mu(){
    draw = false;
    if(pencil.checked || eraser.checked){
        c.closePath();
    }else if(line.checked || triangle.checked || circle.checked || rectangle.checked){
        if(!imgDraw) _history.undo_list.pop();
    }else if(text.checked ){
        _history.redo_list = [];
        _history.undo_list.pop();
    }
}

function text_outf(){
    if(text.checked){
        _history.reg_list = [];
        _history.undo(canvas, c);
        _history.redo_list.pop();
    }
}