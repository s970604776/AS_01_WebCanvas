# Software Studio 2018 Spring Assignment 01 Web Canvas

## Web Canvas
<img src="example01.gif" width="700px" height="500px"></img>

## Todo
1. **Fork the repo ,remove fork relationship and change project visibility to public.**
2. Create your own web page with HTML5 canvas element where we can draw somethings.
3. Beautify appearance (CSS).
4. Design user interaction widgets and control tools for custom setting or editing (JavaScript).
5. **Commit to "your" project repository and deploy to Gitlab page.**
6. **Describing the functions of your canvas in REABME.md**

## Scoring (Check detailed requirments via iLMS)

| **Item**                                         | **Score** |
| :----------------------------------------------: | :-------: |
| Basic components                                 | 60%       |
| Advance tools                                    | 35%       |
| Appearance (subjective)                          | 5%        |
| Other useful widgets (**describe on README.md**) | 1~10%     |

## Reminder
* Do not make any change to our root project repository.
* Deploy your web page to Gitlab page, and ensure it works correctly.
    * **Your main page should be named as ```index.html```**
    * **URL should be : https://[studentID].gitlab.io/AS_01_WebCanvas**
* You should also upload all source code to iLMS.
    * .html or .htm, .css, .js, etc.
    * source files
* **Deadline: 2018/04/05 23:59 (commit time)**
    * Delay will get 0 point (no reason)
    * Copy will get 0 point
    * "屍體" and 404 is not allowed

---

## Put your report below here

#### **1.  基本控制功具**
#####   筆刷、橡皮擦
*   可以調整大小
*   能更改顏色（橡皮擦不能）
#####   顏色選擇器
*   系統內建調色盤
*   手動輸入色碼（簡單判斷色碼長度正確性）
*   調色盤和色碼互相同步
#####   文字輸入
*   獨立選單（當使用者選擇了文字輸入，選單才會出現）
*   文字顏色、大小、字型、粗體
*   即時預覽
#####   鼠標造型
*   筆（筆刷）
*   十字架（其他）
#####   清除畫面
*   整個canvas變為白色
*   不能 還原／重做
#### **2.  建議功能**
#####   圖型
*   即時控制和預覽
*   圓
    +   透過滑鼠控制大小（X、Y軸都可以）
*   矩形
*   三角形
#####   還原／重做
*   所有步驟／筆跡都可還原
*   還原（undo）後再操作，不能重做（redo）
#####   上傳／下載圖片
*   可正常操作（下載檔名為"canvas_image.png"）
#### **3.  外觀**
#####   `Simple and luxury design`
#### **4.  其他功能**
#####   兩個選單（主要和文字）
*   可以隨意移動
#####   白色背景
*   將canvas背景設成白色
*   有利於下載後的圖片
#####   直線
*   即時預覽
*   線條大小、顏色
